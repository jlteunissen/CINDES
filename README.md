
This is the CINDES package containing two modules:
  1. evaluation: HPC automation of job submission/processing
  2. algorithms: Several algorithms to perform combinatorial optimizations. The most important ones are the Best-First-Search algorithm and several Genetic Algorithm implementations (as well as NSGA-II), and Bayesian optimization

A manual is available, please email to jlteunissen@gmail.com

to run the test suite:
$> nosetests
